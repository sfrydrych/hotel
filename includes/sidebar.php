<nav class="ts-sidebar">
	<ul class="ts-sidebar-menu">	
		<li class="ts-label">Strona Główna</li>
		<?PHP if(isset($_SESSION['id']))
			{ ?>
				<li><a href="dashboard.php"><i class="fa fa-desktop"></i>Pulpit Zarządzania</a></li>
				<li><a href="my-profile.php"><i class="fa fa-user"></i>Mój Profil</a></li>
				<li><a href="change-password.php"><i class="fa fa-files-o"></i>Zmiana Hasła</a></li>
				<li><a href="book-room.php"><i class="fa fa-file-o"></i>Rezerwacja Pokoju</a></li>
				<li><a href="room-details.php"><i class="fa fa-file-o"></i>Szczegóły Pokoju</a></li>
			<?php } else { ?>
				<li><a href="registration.php"><i class="fa fa-files-o"></i>Rejestracja Użytkownika</a></li>
				<li><a href="index.php"><i class="fa fa-users"></i>Logowanie Użytkownika</a></li>
				<li><a href="admin"><i class="fa fa-user"></i>Logowanie Pracownika</a></li>
			<?php } ?>
	</ul>
</nav>