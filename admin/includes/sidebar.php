<nav class="ts-sidebar">
	<ul class="ts-sidebar-menu">
		<li class="ts-label">Strona Główna</li>
		<li><a href="dashboard.php"><i class="fa fa-dashboard"></i>Pulpit Zarządzania</a></li>
			<li><a href="#"><i class="fa fa-desktop"></i>Pokoje</a>
			<ul>
				<li><a href="create-room.php">Dodaj Pokój</a></li>
				<li><a href="manage-rooms.php">Zarządzaj Pokojami</a></li>
			</ul>
		</li>
		<li><a href="reservation.php"><i class="fa fa-user"></i>Rezerwacja Pokoju</a></li>
		<li><a href="manage-reservations.php"><i class="fa fa-users"></i>Zarządzaj Rezerwacjami</a></li>	
</nav>