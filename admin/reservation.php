<?php
session_start();
include('includes/config.php');
include('includes/checklogin.php');
check_login();

if($_POST['submit'])
{
$roomno=$_POST['room'];
$seater=$_POST['seater'];
$feespm=$_POST['fpm'];
$stayfrom=$_POST['stayf'];
$duration=$_POST['duration'];
$regno=$_POST['regno'];
$fname=$_POST['fname'];
$lname=$_POST['lname'];
$gender=$_POST['gender'];
$contactno=$_POST['contact'];
$emailid=$_POST['email'];
$paddress=$_POST['paddress'];
$caddress=$_POST['address'];
$query="insert into rezerwacje(numer_pok,ilosc_osob,cena,data_przybycia,czas_pobytu,numer_rej,imie,nazwisko,plec,numer_tel,email,adres,adres_kores) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
$stmt = $mysqli->prepare($query);
$rc=$stmt->bind_param('iiisiisssisss',$roomno,$seater,$feespm,$stayfrom,$duration,$regno,$fname,$lname,$gender,$contactno,$emailid,$paddress,$caddress);
$stmt->execute();
$stmt->close();
$query1="insert into klienci(numer_rej,imie,nazwisko,plec,numer_tel,email,haslo) values(?,?,?,?,?,?,?)";
$stmt1= $mysqli->prepare($query1);
$stmt1->bind_param('ssssiss',$regno,$fname,$lname,$gender,$contactno,$emailid,$contactno);
$stmt1->execute();
echo"<script>alert('Pokój został zarezerwowany pomyślnie');</script>";
}
?>

<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="theme-color" content="#3e454c">
	<title>Rezerwacja Pokoju</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/dataTables.bootstrap.min.css">>
	<link rel="stylesheet" href="css/bootstrap-social.css">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<link rel="stylesheet" href="css/fileinput.min.css">
	<link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css">
	<link rel="stylesheet" href="css/style.css">
<script type="text/javascript" src="js/jquery-1.11.3-jquery.min.js"></script>
<script type="text/javascript" src="js/validation.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script>
function getSeater(val) {
$.ajax({
type: "POST",
url: "get_seater.php",
data:'roomid='+val,
success: function(data){
$('#ilosc_osob').val(data);
}
});

$.ajax({
type: "POST",
url: "get_seater.php",
data:'rid='+val,
success: function(data){
$('#cena').val(data);
}
});
}
</script>
</head>

<body>
	<?php include('includes/header.php');?>
	<div class="ts-main-content">
	<?php include('includes/sidebar.php');?>
	<div class="content-wrapper">
	<div class="container-fluid">
	<div class="row">
	<div class="col-md-12">
	<h2 class="page-title">Rezerwacja</h2>
	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-primary">
	<div class="panel-heading">Wypełnij Wszystkie Dane</div>
	<div class="panel-body">
	<form method="post" action="" class="form-horizontal">									
	
<div class="form-group">
<label class="col-sm-4 control-label"><h4 style="color: green" align="left">Dane dotyczące pokoju</h4> </label>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Numer pokoju:</label>
<div class="col-sm-8">
<select name="room" id="room"class="form-control"  onChange="getSeater(this.value);" onBlur="checkAvailability()" required> 
<option value="">Wybierz pokój...</option>
<?php $query ="SELECT * FROM pokoje";
$stmt2 = $mysqli->prepare($query);
$stmt2->execute();
$res=$stmt2->get_result();
while($row=$res->fetch_object())
{
?>
<option value="<?php echo $row->numer_pok;?>"> <?php echo $row->numer_pok;?></option>
<?php } ?>
</select> 
<span id="room-availability-status" style="font-size:12px;"></span>
</div>
</div>
											
<div class="form-group">
<label class="col-sm-2 control-label">Ilość osób:</label>
<div class="col-sm-8">
<input type="text" name="seater" id="seater" class="form-control"  >
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Dzienna opłata:</label>
<div class="col-sm-8">
<input type="text" name="fpm" id="fpm" class="form-control" >
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Data przybycia:</label>
<div class="col-sm-8">
<input type="date" name="stayf" id="stayf" class="form-control" >
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Czas pobytu:</label>
<div class="col-sm-8">
<select name="duration" id="duration" class="form-control">
<option value="">Wybierz ilość dni...</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
</select>
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label"><h4 style="color: green" align="left">Dane osobowe</h4> </label>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Numer rejestracyjny:</label>
<div class="col-sm-8">
<input type="text" name="regno" id="regno"  class="form-control" required="required" >
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Imię:</label>
<div class="col-sm-8">
<input type="text" name="fname" id="fname"  class="form-control" required="required" >
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Nazwisko:</label>
<div class="col-sm-8">
<input type="text" name="lname" id="lname"  class="form-control" required="required">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Płeć:</label>
<div class="col-sm-8">
<select name="gender" class="form-control" required="required">
<option value="">Wybierz płeć...</option>
<option value="mezczyzna">Mężczyzna</option>
<option value="kobieta">Kobieta</option>
</select>
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Numer telefonu:</label>
<div class="col-sm-8">
<input type="text" name="contact" id="contact"  class="form-control" required="required">
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Email:</label>
<div class="col-sm-8">
<input type="email" name="email" id="email"  class="form-control" required="required">
</div>
</div>


<div class="form-group">
<label class="col-sm-3 control-label"><h4 style="color: green" align="left">Adres zamieszkania</h4> </label>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Dane adresowe:</label>
<div class="col-sm-8">
<textarea  rows="5" name="paddress" id="paddress" class="form-control" required="required"></textarea>
</div>
</div>							


<div class="form-group">
<label class="col-sm-3 control-label"><h4 style="color: green" align="left">Adres korespondencyjny</h4> </label>
</div>

<div class="form-group">
<label class="col-sm-5 control-label">Adres korespondencyjny taki sam jak adres zamieszkania:</label>
<div class="col-sm-4">
<input type="checkbox" name="adcheck" value="1"/>
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Dane adresowe:</label>
<div class="col-sm-8">
<textarea  rows="5" name="address" id="address" class="form-control" required="required"></textarea>
</div>
</div>
								

<div class="col-sm-6 col-sm-offset-4">
<button class="btn btn-default" type="submit">Anuluj</button>
<input type="submit" name="submit" Value="Rezerwuj" class="btn btn-primary">
</div>

	</form>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div> 	

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
	<script src="js/dataTables.bootstrap.min.js"></script>
	<script src="js/Chart.min.js"></script>
	<script src="js/fileinput.js"></script>
	<script src="js/chartData.js"></script>
	<script src="js/main.js"></script>
</body>

<script type="text/javascript">
	$(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
                $('#address').val( $('#paddress').val() );
            } 
        });
    });
</script>

<script>
function checkAvailability() {
$("#loaderIcon").show();
jQuery.ajax({
url: "check_availability.php",
data:'roomno='+$("#room").val(),
type: "POST",
success:function(data){
$("#room-availability-status").html(data);
$("#loaderIcon").hide();
},
error:function (){}
});
}
</script>

</html>