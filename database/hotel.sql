-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2016 at 09:41 PM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `pracownicy`
--

CREATE TABLE IF NOT EXISTS `pracownicy` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `haslo` varchar(300) NOT NULL,
  `data_rej` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktual` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pracownicy`
--

INSERT INTO `pracownicy` (`id`, `login`, `email`, `haslo`, `data_rej`, `data_aktual`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin', '2018-12-12 20:31:45', '2018-12-17');

-- --------------------------------------------------------

--
-- Table structure for table `rezerwacje`
--

CREATE TABLE IF NOT EXISTS `rezerwacje` (
  `id` int(11) NOT NULL,
  `numer_pok` int(11) NOT NULL,
  `ilosc_osob` int(11) NOT NULL,
  `cena` int(11) NOT NULL,
  `data_przybycia` date NOT NULL,
  `czas_pobytu` int(11) NOT NULL,
  `numer_rej` int(11) NOT NULL,
  `imie` varchar(500) NOT NULL,
  `nazwisko` varchar(500) NOT NULL,
  `plec` varchar(250) NOT NULL,
  `numer_tel` bigint(11) NOT NULL,
  `email` varchar(500) NOT NULL,
  `adres` varchar(500) NOT NULL,
  `adres_kores` varchar(500) NOT NULL,
  `data_rezer` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pokoje`
--

CREATE TABLE IF NOT EXISTS `pokoje` (
  `id` int(11) NOT NULL,
  `ilosc_osob` int(11) NOT NULL,
  `numer_pok` int(11) NOT NULL,
  `cena` int(11) NOT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokoje`
--

INSERT INTO `pokoje` (`id`, `ilosc_osob`, `numer_pok`, `cena`, `data_dodania`) VALUES
(1, 4, 100, 80, '2018-04-11 22:45:43'),
(2, 3, 201, 90, '2018-04-12 01:30:47'),
(3, 3, 200, 90, '2018-04-12 01:30:58'),
(4, 2, 112, 140, '2018-04-12 01:31:07'),
(5, 2, 132, 200, '2018-04-12 01:31:15');

-- --------------------------------------------------------

--
-- Table structure for table `klienci`
--

CREATE TABLE IF NOT EXISTS `klienci` (
  `id` int(11) NOT NULL,
  `numer_rej` varchar(255) NOT NULL,
  `imie` varchar(255) NOT NULL,
  `nazwisko` varchar(255) NOT NULL,
  `plec` varchar(255) NOT NULL,
  `numer_tel` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `haslo` varchar(255) NOT NULL,
  `data_rej` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aktual` varchar(45) NOT NULL,
  `data_aktual_hasla` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `klienci`
--

INSERT INTO `klienci` (`id`, `numer_rej`, `imie`, `nazwisko`, `plec`, `numer_tel`, `email`, `haslo`, `data_rej`, `data_aktual`, `data_aktual_hasla`) VALUES
(19, '102355', 'Henryk', 'Zamojski', 'mezczyzna', 6786786786, 'henryk@gmail.com', 'hen1', '2016-06-26 16:33:36', '', ''),
(20, '586952', 'Benjamin', 'Piotrowski', 'mezczyzna', 8596185625, 'benjamin@gmail.com', 'ben1', '2016-06-26 16:40:07', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `pracownicy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `rezerwacje`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `pokoje`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userregistration`
--
ALTER TABLE `klienci`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `pracownicy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `rezerwacje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `pokoje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `userregistration`
--
ALTER TABLE `klienci`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
