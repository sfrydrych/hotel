<?php
session_start();
include('includes/config.php');
include('includes/checklogin.php');
check_login();
?>
<!doctype html>
<html lang="en" class="no-js">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="theme-color" content="#3e454c">
	<title>Szczegóły Pokoju</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-social.css">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<link rel="stylesheet" href="css/fileinput.min.css">
	<link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css">
	<link rel="stylesheet" href="css/style.css">
<script language="javascript" type="text/javascript">
var popUpWin=0;
function popUpWindow(URLStr, left, top, width, height)
{
 if(popUpWin)
{
if(!popUpWin.closed) popUpWin.close();
}
popUpWin = open(URLStr,'popUpWin', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width='+510+',height='+430+',left='+left+', top='+top+',screenX='+left+',screenY='+top+'');
}

</script>

</head>

<body>
	<?php include('includes/header.php');?>

	<div class="ts-main-content">
		<?php include('includes/sidebar.php');?>
		<div class="content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<br/>
						<h2 class="page-title">Szczegóły Pokoju </h2>
						<div class="panel panel-default">
							<div class="panel-heading">Wszystkie Szczegółowe Informacje</div>
							<div class="panel-body">
								<table id="zctb" class="table table-bordered " cellspacing="0" width="100%">
									<tbody>
<?php	
$aid=$_SESSION['login'];
$ret="select * from rezerwacje where email=?";
$stmt= $mysqli->prepare($ret) ;
$stmt->bind_param('s',$aid);
$stmt->execute() ;
$res=$stmt->get_result();
$cnt=1;
while($row=$res->fetch_object())
{
?>

<tr>
<td colspan="4"><h4>Informacje dotyczące pokoju</h4></td>
<tr>
<td colspan="6"><b>Data rezerwacji: <?php echo $row->data_rezer;?></b></td>
</tr>

<tr>
<td><b>Numer pokoju:</b></td>
<td><?php echo $row->numer_pok;?></td>
<td><b>Ilość osób:</b></td>
<td><?php echo $row->ilosc_osob;?></td>
<td><b>Dzienna opłata:</b></td>
<td><?php echo $fpm=$row->cena;?></td>
</tr>

<tr>
<td><b>Data przybycia:</b></td>
<td><?php echo $row->data_przybycia;?></td>
<td><b>Czas pobytu:</b></td>
<td><?php echo $dr=$row->czas_pobytu;?> dni</td>
<td><b>Koszt całkowity:</b></td> 
<td><?php echo $dr*$fpm;?></td>
</tr>

<tr>
<td colspan="6"><h4>Dane osobowe</h4></td>
</tr>

<tr>
<td><b>Numer rejestracyjny:</b></td>
<td><?php echo $row->numer_rej;?></td>
<td><b>Imię i nazwisko:</b></td>
<td><?php echo $row->imie;?><?php echo " ";?><?php echo $row->nazwisko;?></td>
<td><b>Email:</b></td>
<td><?php echo $row->email;?></td>
</tr>

<tr>
<td><b>Numer telefonu:</b></td>
<td><?php echo $row->numer_tel;?></td>
<td><b>Płeć:</b></td>
<td><?php echo $row->plec;?></td>
</tr>

<tr>
<td colspan="6"><h4>Adres</h4></td>
</tr>
<tr>
<td><b>Adres zamieszkania</b></td>
<td colspan="2">
<?php echo $row->adres;?>
</td>

<td><b>Adres korespondencyjny</b></td>
<td colspan="2">
<?php echo $row->adres_kores;?>	
</td>
</tr>


<?php
$cnt=$cnt+1;
} ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
	<script src="js/dataTables.bootstrap.min.js"></script>
	<script src="js/Chart.min.js"></script>
	<script src="js/fileinput.js"></script>
	<script src="js/chartData.js"></script>
	<script src="js/main.js"></script>

</body>

</html>