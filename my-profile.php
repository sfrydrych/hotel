<?php
session_start();
include('includes/config.php');
date_default_timezone_set('Europe/Warsaw');
include('includes/checklogin.php');
check_login();
$aid=$_SESSION['id'];
if(isset($_POST['update']))
{

$regno=$_POST['regno'];
$fname=$_POST['fname'];
$lname=$_POST['lname'];
$gender=$_POST['gender'];
$contactno=$_POST['contact'];
$udate = date('d-m-Y h:i:s', time());
$query="update klienci set numer_rej=?,imie=?,nazwisko=?,plec=?,numer_tel=?,data_aktual=? where id=?";
$stmt = $mysqli->prepare($query);
$rc=$stmt->bind_param('ssssisi',$regno,$fname,$lname,$gender,$contactno,$udate,$aid);
$stmt->execute();
echo"<script>alert('Profil został zaktualizowany pomyślnie');</script>";
}
?>

<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="theme-color" content="#3e454c">
	<title>Aktualizacja Profilu</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/dataTables.bootstrap.min.css">>
	<link rel="stylesheet" href="css/bootstrap-social.css">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<link rel="stylesheet" href="css/fileinput.min.css">
	<link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css">
	<link rel="stylesheet" href="css/style.css">
<script type="text/javascript" src="js/jquery-1.11.3-jquery.min.js"></script>
<script type="text/javascript" src="js/validation.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
function valid()
{
if(document.registration.password.value!= document.registration.cpassword.value)
{
alert("Pola hasło i ponów hasło różnią się!");
document.registration.cpassword.focus();
return false;
}
return true;
}
</script>
</head>
<body>
	<?php include('includes/header.php');?>
	<div class="ts-main-content">
		<?php include('includes/sidebar.php');?>
		<div class="content-wrapper">
			<div class="container-fluid">
	<?php	
	$aid=$_SESSION['id'];
	$ret="select * from klienci where id=?";
	$stmt= $mysqli->prepare($ret) ;
	$stmt->bind_param('i',$aid);
	$stmt->execute();
	$res=$stmt->get_result();
	//$cnt=1;
	while($row=$res->fetch_object())
		{
	?>	
			<div class="row">
				<div class="col-md-12">
					<h2 class="page-title"><?php echo $row->imie;?>&nbsp;- Profil </h2>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary">
								<div class="panel-heading">
									Data ostatniej aktualizacji: &nbsp; <?php echo $row->data_aktual;?> 
</div>		
<div class="panel-body">
<form method="post" action="" name="registration" class="form-horizontal" onSubmit="return valid();">
<div class="form-group">
<label class="col-sm-2 control-label"> Numer rejestracyjny: </label>
<div class="col-sm-8">
<input type="text" name="regno" id="regno"  class="form-control" required="required" value="<?php echo $row->numer_rej;?>" >
</div>
</div>


<div class="form-group">
<label class="col-sm-2 control-label">Imię: </label>
<div class="col-sm-8">
<input type="text" name="fname" id="fname"  class="form-control" value="<?php echo $row->imie;?>" required="required" >
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Nazwisko: </label>
<div class="col-sm-8">
<input type="text" name="lname" id="lname"  class="form-control" value="<?php echo $row->nazwisko;?>" required="required">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Płeć: </label>
<div class="col-sm-8">
<select name="gender" class="form-control" required="required">
<option value="<?php echo $row->plec;?>"><?php echo $row->plec;?></option>
<option value="mezczyzna">Mężczyzna</option>
<option value="kobieta">Kobieta</option>
</select>
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Numer telefonu: </label>
<div class="col-sm-8">
<input type="text" name="contact" id="contact" class="form-control" maxlength="10" value="<?php echo $row->numer_tel;?>" required="required">
</div>
</div>

<div class="form-group">
<label class="col-sm-2 control-label">Email: </label>
<div class="col-sm-8">
<input type="email" name="email" id="email"  class="form-control" value="<?php echo $row->email;?>" readonly>
<span id="user-availability-status" style="font-size:12px;"></span>
</div>
</div>
<?php } ?>

<div class="col-sm-6 col-sm-offset-4">
<input type="submit" name="update" Value="Aktualizuj Profil" class="btn btn-primary">
</div>
</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 	

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
	<script src="js/dataTables.bootstrap.min.js"></script>
	<script src="js/Chart.min.js"></script>
	<script src="js/fileinput.js"></script>
	<script src="js/chartData.js"></script>
	<script src="js/main.js"></script>
</body>

<script>
function checkAvailability() {

$("#loaderIcon").show();
jQuery.ajax({
url: "check_availability.php",
data:'emailid='+$("#email").val(),
type: "POST",
success:function(data){
$("#user-availability-status").html(data);
$("#loaderIcon").hide();
},
error:function (){}
});
}
</script>

</html>