<?php
require_once("includes/config.php");
if(!empty($_POST["emailid"])) {
	$email= $_POST["emailid"];
	if (filter_var($email, FILTER_VALIDATE_EMAIL)===false) {
		echo "Błąd : Wprowadzono nieprawidłowy email.";
	}
	else {
		$result ="SELECT count(*) FROM klienci WHERE email=?";
		$stmt = $mysqli->prepare($result);
		$stmt->bind_param('s',$email);
		$stmt->execute();
		$stmt->bind_result($count);
		$stmt->fetch();
		$stmt->close();
		if($count>0)
		{
			echo "<span style='color:red'> Ten email już istnieje.</span>";
		}
		else {
			echo "<span style='color:green'> Email dostępny do rejestracji.</span>";
		}
	}
}

if(!empty($_POST["oldpassword"])) 
{
$pass=$_POST["oldpassword"];
$result ="SELECT haslo FROM klienci WHERE haslo=?";
$stmt = $mysqli->prepare($result);
$stmt->bind_param('s',$pass);
$stmt->execute();
$stmt -> bind_result($result);
$stmt -> fetch();
$opass=$result;
if($opass==$pass) 
	echo "<span style='color:green'> Hasło zgodne.</span>";
else
	echo "<span style='color:red'> Hasło nie jest zgodne.</span>";
}


if(!empty($_POST["roomno"])) 
{
$roomno=$_POST["roomno"];
$result ="SELECT count(*) FROM rezerwacje WHERE numer_pok=?";
$stmt = $mysqli->prepare($result);
$stmt->bind_param('i',$roomno);
$stmt->execute();
$stmt->bind_result($count);
$stmt->fetch();
$stmt->close();
if($count>0)
	echo "<span style='color:red'>$count. Wszystkie miejsca są zajęte.</span>";
else
	echo "<span style='color:red'>Wszystkie miejsca są dostępne.</span>";
}
?>